﻿#include <iostream>
#include <sstream>

template<typename T>
bool defaultComparator(const T& a, const T& b) {
    return a > b;
}
std::string intToString(int data) {
    return std::to_string(data);
}

template <typename T> class BinaryHeap {
private:

    T* heapArray = new T[capacity];
    int capacity = 10;
    int size;

    void Resize() {
        capacity *= 2;
        T* newArray = new T[capacity];
        std::copy(heapArray, heapArray + size, newArray);
        delete[] heapArray;
        heapArray = newArray;
    }

    /*void heapifyUp(int index, bool (*comparator)(const T&, const T&)) {
        int parent = (index - 1) / 2;
        while (index > 0 && comparator(heapArray[index], heapArray[parent])) {
            std::swap(heapArray[index], heapArray[parent]);
            index = parent;
            parent = (index - 1) / 2;
        }
    }*/

    void HeapifyUp(int index, bool (*comparator)(const T&, const T&)) {
        int parent = (index - 1) / 2;
        if (parent >= 0 && comparator(heapArray[index], heapArray[parent])) {
            std::swap(heapArray[index], heapArray[parent]);
            HeapifyUp(parent, comparator);
        }
        /*int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;
        int largest = index;

        if (leftChild < size && comparator(heapArray[leftChild], heapArray[largest]))
            largest = leftChild;

        if (rightChild < size && comparator(heapArray[rightChild], heapArray[largest]))
            largest = rightChild;

        if (largest != index) {
            std::swap(heapArray[index], heapArray[largest]);
            HeapifyDown(largest,comparator);
        }*/
    }/*

    void heapifyUpRecursive(int index, bool (*comparator)(const T&, const T&)) {
        if (index > 0) {
            int parent = (index - 1) / 2;
            if (comparator(heapArray[index], heapArray[parent])) {
                std::swap(heapArray[index], heapArray[parent]);
                heapifyUpRecursive(parent,comparator);
            }
        }
    }*/

    void HeapifyDown(int index, bool (*comparator)(const T&, const T&)) {
        int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;
        int largest;

        if (comparator(heapArray[leftChild], heapArray[rightChild]))
            largest = leftChild;
        else largest = rightChild;

        if (comparator(heapArray[largest], heapArray[index])) {
            std::swap(heapArray[index], heapArray[largest]);
            HeapifyDown(largest, comparator);
        }
    }

public:
    void Insert(const T& data, bool (*comparator)(const T&, const T&)) {
        if (size == capacity) {
            Resize();
        }

        heapArray[size++] = data;
        HeapifyUp(size - 1, comparator);
    }

    T RemoveMax(bool (*comparator)(const T&, const T&)) {
        if (size == 0) {
            return NULL;
        }

        T temp = heapArray[0];

        heapArray[0] = heapArray[--size];
        HeapifyDown(0, comparator);

        return temp;
    }

    void Clear() {
        delete[] heapArray;
        capacity = 10;
        size = 0;
        heapArray = new T[capacity];
    }


    void HeapifyUpFromIndex(int index, bool (*comparator)(const T&, const T&)) {
        HeapifyUp(index, comparator);
    }

    void HeapifyDownFromIndex(int index, bool (*comparator)(const T&, const T&)) {
        HeapifyDown(index, comparator);
    }

    std::string ToString() {
        return "Array has " + std::to_string(size) + " item(s).";
    }
    std::string ToString(std::string(*DataToString)(T)) {
        std::string finalText = "Array has " + std::to_string(size) + " item(s).";
        finalText += "\nFirst item: " + DataToString(heapArray[0]);
        if (size > 1) finalText += "\nLast item: " + DataToString(heapArray[size - 1]);

        return finalText;
    }
    std::string ToStringFull(std::string(*DataToString)(T)) {
        std::string finalText = "Array has " + std::to_string(size) + " item(s).";
        for (int i = 0; i < size; ++i) {
            finalText += "\nItem" + std::to_string(i) + ": " + DataToString(heapArray[i]);
        }

        return finalText;
    }

    //std::string to_string(int num_elements = 10) {
    //    //std::stringstream ss;
    //    std::cout << "DynamicArray (Size: " << size << ", Capacity: " << capacity << ")\n";
    //    int print_count = std::min(size, num_elements);
    //    for (int i = 0; i < print_count; ++i) {

    //        std::cout << heapArray[i] << "\n";
    //    }
    //    return ss.str();
    //}

};

int main() {
    BinaryHeap<int> maxHeap;

    maxHeap.Insert(26, defaultComparator);
    maxHeap.Insert(16, defaultComparator);
    maxHeap.Insert(8, defaultComparator);
    maxHeap.Insert(10, defaultComparator);
    maxHeap.Insert(17, defaultComparator);
    //   maxHeap.HeapifyDownFromIndex(2);
    std::cout << maxHeap.ToStringFull(intToString) << std::endl;
    maxHeap.RemoveMax(defaultComparator);
    std::cout << maxHeap.ToStringFull(intToString) << std::endl;


    /*
    maxHeap.Insert(20);
    maxHeap.Insert(20);*/

    //// Wywo�anie funkcji przekopywania rekurencyjnego w g�r� od indeksu 2
    //maxHeap.heapifyUpFromIndex(2);

    //// Wywo�anie funkcji przekopywania rekurencyjnego w d� od indeksu 0
    //maxHeap.HeapifyDownFromIndex(0);

    //int maxData;
    //std::string maxInfo;

    //while (maxHeap.extractMax(maxData, maxInfo)) {
    //    std::cout << "Max Data: " << maxData << ", Max Info: " << maxInfo << std::endl;
    //}

    return 0;
}
